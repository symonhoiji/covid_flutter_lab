import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccines = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadVaccines();
  }

  Future<void> _loadVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _saveVaccines() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccines', vaccines);
    });
  }

  Widget _vaccineComboBox(
      {required String title,
      required String value,
      ValueChanged<String?>? onChanged}) {
    return Row(
      children: [
        Text(title),
        Spacer(),
        DropdownButton(value: value, onChanged: onChanged, items: [
          DropdownMenuItem(child: Text('-'), value: '-'),
          DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
          DropdownMenuItem(
              child: Text('Johnson & Johnson'), value: 'Johnson & Johnson'),
          DropdownMenuItem(child: Text('Sputnik V'), value: 'Sputnik V'),
          DropdownMenuItem(child: Text('AstraZeneca'), value: 'AstraZeneca'),
          DropdownMenuItem(child: Text('Novavax'), value: 'Novavax'),
          DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm'),
          DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac')
        ])
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Vaccine'),
              Text(
                'เลือกวัคซีน',
                style: TextStyle(fontWeight: FontWeight.w300, fontSize: 13.0),
              )
            ],
          ),
          backgroundColor: Colors.red[700],
        ),
        body: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              Text('$vaccines'),
              _vaccineComboBox(
                title: 'เข็ม 1',
                value: vaccines[0],
                onChanged: (String? newValue) {
                  setState(() {
                    vaccines[0] = newValue!;
                  });
                },
              ),
              _vaccineComboBox(
                title: 'เข็ม 2',
                value: vaccines[1],
                onChanged: (String? newValue) {
                  setState(() {
                    vaccines[1] = newValue!;
                  });
                },
              ),
              _vaccineComboBox(
                title: 'เข็ม 3',
                value: vaccines[2],
                onChanged: (String? newValue) {
                  setState(() {
                    vaccines[2] = newValue!;
                  });
                },
              ),
              SizedBox(
                width: double.infinity,
                height: 60.0,
                child: ElevatedButton(
                    onPressed: () {
                      _saveVaccines();
                      Navigator.pop(context);
                    },
                    child: Text('Save')),
              ),
            ],
          ),
        ));
  }
}

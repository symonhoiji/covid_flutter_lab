import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questionValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var question = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดหล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Question'),
            Text(
              'เลือกคำถามเพื่อวัดอาการ',
              style: TextStyle(fontWeight: FontWeight.w300, fontSize: 13.0),
            )
          ],
        ),
        backgroundColor: Colors.blue[700],
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: [
            CheckboxListTile(
              value: questionValues[0],
              title: Text(question[0]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[0] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[1],
              title: Text(question[1]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[1] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[2],
              title: Text(question[2]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[2] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[3],
              title: Text(question[3]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[3] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[4],
              title: Text(question[4]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[4] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[5],
              title: Text(question[5]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[5] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[6],
              title: Text(question[6]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[6] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[7],
              title: Text(question[7]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[7] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[8],
              title: Text(question[8]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[8] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[9],
              title: Text(question[9]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[9] = newValue!;
                });
              },
            ),
            CheckboxListTile(
              value: questionValues[10],
              title: Text(question[10]),
              onChanged: (newValue) {
                setState(() {
                  questionValues[10] = newValue!;
                });
              },
            ),
            SizedBox(
              width: double.infinity,
              height: 60.0,
              child: ElevatedButton(
                  onPressed: () async {
                    await _saveQuestion();
                    Navigator.pop(context);
                  },
                  child: Text('Save')),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQVS = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrStrQuestionValues =
        strQVS.substring(1, strQVS.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        questionValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionValues.toString());
  }
}

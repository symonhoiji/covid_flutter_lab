import 'package:covid_app/profile.dart';
import 'package:covid_app/question.dart';
import 'package:covid_app/risk.dart';
import 'package:covid_app/vaccine.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(
    MaterialApp(
      title: 'Covid-19',
      home: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M'; // M,F
  int age = 0;
  var questionScore = 0.0;
  var riskScore = 0.0;
  var vaccines = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQVS = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrStrQuestionValues =
        strQVS.substring(1, strQVS.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        if (arrStrQuestionValues[i].trim() == 'true') {
          questionScore = questionScore + 1;
        }
      }
      questionScore = (100 * questionScore) / 11;
    });
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRVS = prefs.getString('risk_values') ??
        '[false, false, false, false, false, false, false]';
    var arrStrRiskValues = strRVS.substring(1, strRVS.length - 1).split(',');
    setState(() {
      riskScore = 0.0;
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        if (arrStrRiskValues[i].trim() == 'true') {
          riskScore++;
        }
      }
      riskScore = (100 * riskScore) / 7;
    });
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    _loadQuestion();
    _loadRisk();
    setState(() {
      name = prefs.getString('name') ?? '';
      age = prefs.getInt('age') ?? 0;
      gender = prefs.getString('gender') ?? 'M';
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('age');
    prefs.remove('gender');
    prefs.remove('vaccines');
    prefs.remove('question_values');
    prefs.remove('risk_values');
    await _loadProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main'),
      ),
      body: Container(
        padding: EdgeInsets.all(8.0),
        child: ListView(
          children: [
            Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                    title: (name != '')
                        ? Text(
                            name,
                            style: TextStyle(color: Colors.white),
                          )
                        : Text(
                            'ยังไม่ได้ระบุชื่อ',
                            style: TextStyle(color: Colors.white),
                          ),
                    tileColor: Colors.blueAccent,
                    subtitle: (age == 0)
                        ? Text(
                            'เพศ: $gender, อายุ ยังไม่ได้ระบุ ปี',
                            style: TextStyle(color: Colors.white),
                          )
                        : Text(
                            'เพศ: $gender, อายุ $age ปี',
                            style: TextStyle(color: Colors.white),
                          ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      'อาการ ${questionScore.toStringAsFixed(2)} %',
                      style: TextStyle(
                          fontWeight: (questionScore > 30)
                              ? FontWeight.bold
                              : FontWeight.normal,
                          fontSize: 24.0,
                          color: (questionScore > 30)
                              ? Colors.red.withOpacity(0.6)
                              : Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      'ความเสี่ยง ${riskScore.toStringAsFixed(2)} %',
                      style: TextStyle(
                          fontWeight: (riskScore > 30)
                              ? FontWeight.bold
                              : FontWeight.normal,
                          fontSize: 24.0,
                          color: (riskScore > 30)
                              ? Colors.red.withOpacity(0.6)
                              : Colors.black.withOpacity(0.6)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: [
                        Text(
                          'ได้รับการฉีด',
                          style:
                              TextStyle(color: Colors.black.withOpacity(0.6)),
                        ),
                        Text(
                          '${vaccines[0]}',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          '${vaccines[1]}',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600),
                        ),
                        Text(
                          '${vaccines[2]}',
                          style: TextStyle(
                              color: Colors.black.withOpacity(0.6),
                              fontSize: 16.0,
                              fontWeight: FontWeight.w600),
                        ),
                      ],
                    ),
                  ),
                  ButtonBar(
                    alignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {
                          setState(() {
                            _resetProfile();
                          });
                        },
                        child: const Text('Reset'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ProfileWidget()));
                await _loadProfile();
              },
              child: Card(
                color: Colors.amber[700],
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(
                        Icons.account_box,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Profile',
                        style: TextStyle(color: Colors.white),
                      ),
                      subtitle: Text(
                        'จัดการแก้ไขข้อมูลส่วนตัว',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => VaccineWidget()));
                await _loadProfile();
              },
              child: Card(
                color: Colors.red[700],
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(
                        Icons.local_hospital,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Vaccine',
                        style: TextStyle(color: Colors.white),
                      ),
                      subtitle: Text(
                        'เลือกวัคซีน',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => QuestionWidget()));
                await _loadProfile();
              },
              child: Card(
                color: Colors.blue[700],
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(
                        Icons.question_answer,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Question',
                        style: TextStyle(color: Colors.white),
                      ),
                      subtitle: Text(
                        'เลือกคำถามเพื่อวัดอาการ',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RiskWidget()));
                await _loadProfile();
              },
              child: Card(
                color: Colors.black,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      leading: Icon(
                        Icons.announcement,
                        color: Colors.white,
                      ),
                      title: Text(
                        'Risk',
                        style: TextStyle(color: Colors.white),
                      ),
                      subtitle: Text(
                        'เลือกคำถามเพื่อวัดความเสี่ยง',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
